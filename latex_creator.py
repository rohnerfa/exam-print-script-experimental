import re, os, sys, subprocess
import time
from subprocess import Popen, PIPE, STDOUT
from collections import defaultdict
from pathlib import Path


with open(Path(os.environ.get('EXAM_PRINT_SCRIPT_SOURCE_DIR'), 'header.txt'), 'r') as f:
    texheader = ''.join(f.readlines())


class tex_creation_error(Exception):
    pass


texfooter = '\end{document}'

symlink_prefix = '_____'

# used to grep with command `file`
image_formats = ['PNG image data', 'JPEG image data', 'PDF document']
code_formats = ['Python script', 'C source', 'C header', 'C++ source',
                'C++ header', 'MATLAB', 'C++ source file', 'C source file']
text_formats = ['ASCII text', 'UTF-8 Unicode text']

# Sometimes "file" fails and recognises a ".cpp" file as "C source",
# we fix it by setting everything to CPP (who uses C anyways?)
short_names = {'PNG image data': 'PNG',
               'JPEG image data': 'JPG',
               'MATLAB': 'MATLAB',
               'PDF document': 'PDF',
               'Python script': 'PY',
               'ASCII text': 'TXT',
               'UTF-8 Unicode text': 'TXT',
               'C source': 'CPP',
               'C header': 'HPP',
               'C++ source': 'CPP',
               'C++ header': 'HPP',
               'C++ source file': 'CPP',
               'C source file': 'CPP',
               None: 'None'}

# Set listing language to the one on the right
listing_names = {'Python script': 'python',
                 'C source': 'C++',
                 'C header': 'C++',
                 'C++ source': 'C++',
                 'C++ header': 'C++',
                 'C++ source file': 'C++',
                 'C source file': 'C++',
                 'ASCII text': '',
                 'UTF-8 Unicode text': '',
                 'MATLAB': 'Matlab',
                 None: 'None'}

# Set listing language style to the one on the right
style_names = {'Python script': 'PY',
               'MATLAB': 'MATLAB',
               'C source': 'CPP',
               'C header': 'CPP',
               'C++ source file': 'CPP',
               'C source file': 'CPP',
               'C++ source': 'CPP',
               'C++ header': 'CPP',
               'ASCII text': '',
               'UTF-8 Unicode text': '',
               None: 'None'}

# File names that must be ignored
invalid_filenames = [
    lambda x: bool(x.endswith('~')),  # Autosaved junk
    lambda x: x.find('#') != -1,  # ???
    #lambda x: bool(x.endswith('.autosave')),  # Eclipse autosave file ignored
    lambda x: bool(x.startswith('.')),  # Hidden files (blame Eclipse)
    lambda x: bool(x.endswith('.cmake')),  # CMake junk
    lambda x: bool('CMake' in x),  # CMake junk
    lambda x: bool('cmake_install' in x),  # CMake junk
    lambda x: bool('Makefile' in x),  # CMake junk
    lambda x: bool(x.endswith('.cbp')),  # Code::Blocks project file
    lambda x: bool(x.startswith('CMake')),  # Hidden files (blame Eclipse)
]


def is_file(fname, ftype):
    p1 = Popen(["file", fname], stdout=PIPE)
    p2 = Popen(["grep", ftype], stdin=p1.stdout, stdout=PIPE)
    p2.wait()

    return ftype if p2.poll() == 0 else None


def get_ftype(fname):
    code_matches = filter(lambda x: x is not None,
                          map(lambda x: is_file(fname, x), code_formats))
    image_matches = filter(lambda x: x is not None,
                           map(lambda x: is_file(fname, x), image_formats))
    text_matches = filter(lambda x: x is not None,
                          map(lambda x: is_file(fname, x), text_formats))
    sym_link = [is_file(fname, 'symbolic link')]

    if image_matches and code_matches:
        # this is most likely not a text file
        code_matches = []

    # unix file command does not always recognize python scripts
    if text_matches and fname[-3:] == '.py':
        text_matches = []
        code_matches = ['Python script']
    if text_matches and fname[-4:] == '.cpp':
        text_matches = []
        code_matches = ['C++ source file']
    if text_matches and fname[-2:] == '.c':
        text_matches = []
        code_matches = ['C source file']

    # hint: it seems that matlab files are never recognized by the file command
    if text_matches and fname[-2:] == '.m':
        text_matches = []
        code_matches = ['MATLAB']

    alle = list(code_matches) + list(image_matches) + list(
        text_matches) + list(sym_link)

    return alle[0] if alle else None


def md5alias(fname):
    """
    create a symlink with a latex compatible filename
    """
    basename, ending = fname.rsplit('.', 1)
    p1 = Popen(["md5sum", fname], stdout=PIPE)
    if p1.wait() != 0:
        raise Exception("something went wrong...")
    md5sum = str(p1.stdout.read()).split(' ')[0]
    newfname = symlink_prefix + md5sum + '.' + ending

    with open(os.devnull, 'w') as DEVNULL:
        Popen(
            ["ln", "-s", fname, newfname], stdout=DEVNULL,
            stderr=STDOUT).wait()

    return newfname


def md5sum(fname):
    p1 = Popen(["md5sum", fname], stdout=PIPE)
    if p1.wait() != 0:
        raise Exception("something went wrong...")
    md5sum = str(p1.stdout.read()).split(' ')[0]
    return md5sum


def tex_figure(dd, dirname):
    fname = dd['fname']
    fialias = md5alias(fname)
    texstr = ''
    texstr += '\n\\begin{figure}[h!]'
    texstr += '\n   \\centering'
    texstr += '\n   \\includegraphics[width=.7\\linewidth]{%s}' % os.path.join(
        dirname, fialias)
    texstr += '\n   \\caption{ Filename = %s}' % fname.replace('_', '\\_')
    texstr += '\n\\end{figure}'
    return texstr

def modified_lines_option(filename, template_dir):
    template = os.path.join(template_dir, filename)
    cmd = ['diff', '--unchanged-line-format=',
                   '--old-line-format=',
                   "--new-line-format=%dn ",
                   template,
                   filename]
    try:
        print("Running '{}'...".format(" ".join(cmd)))
        mod_lines = subprocess.check_output(cmd)
    except subprocess.CalledProcessError as ret:
        #print("Exception while running diff...")
        mod_lines = ret.output

    #print(mod_lines)
    mod_lines = mod_lines.split(" ")
    mod_lines = [s for s in mod_lines if s != ""]
    #print(mod_lines)

    pattern = '\\ifnum\\value{{lstnumber}}={:s}\\color{{green!40}}\\fi'
    tex = 'linebackgroundcolor={'
    tex = tex + "".join(pattern.format(k) for k in mod_lines)
    tex = tex + '}'
    #print(tex)

    return tex

# dd: dictionary of file informations
def tex_listing(dd):
    fname = dd['fname']

    f = open(fname, 'r')
    texstr = ''
    texstr += ('\n\\begin{lstlisting}[' +
               'caption={%s},' +
               'language={' + listing_names[dd['ftype']] + '},' +
               'style={' + style_names[dd['ftype']] + '},' +
               modified_lines_option(fname, dd['template-dir']) +
               ']\n') % fname.replace('_', r'\_')
    texstr += ''.join(f.readlines())
    texstr += '\n\\end{lstlisting}'
    f.close()
    return texstr


# dd: dictionary of file informations
def tex_verb_listing(dd):
    fname = dd['fname']
    f = open(fname, 'r')
    texstr = ''
    texstr += ('\n\\begin{lstlisting}[caption={%s}, language={' +
               listing_names[dd['ftype']] + '}, style={' +
               style_names[dd['ftype']] + '}]\n') % fname.replace('_', ' ')
    texstr += ''.join(f.readlines())
    texstr += '\n\\end{lstlisting}'
    f.close()
    return texstr


def collect_files(dirname):
    """
    Keyword Arguments:
    dirname -- directory name

    Returns:
    dictionary `section_label` -> list of files
    default value is an empty list
    """
    all_files = []

    for dirpath, subdirs, files in os.walk(dirname):
        all_files += map(lambda x: os.path.join(dirpath, x).lstrip('./'),
                         files)

        # all_files +=
        # # recursion
        # for si in subdirs:
        #     all_files += collect_files(os.path.join(dirname, si))
    all_files = sorted(all_files)

    # create dictionary
    section_ordered_dict = defaultdict(list)
    for fi in all_files:
        fi_splitted = fi.split(os.path.sep)
        index = 0
        etc_label = '_____etc'
        try:
            section_label = fi_splitted[index]
        except:
            section_label = etc_label
        if fi_splitted[-1] == section_label:
            if os.path.islink(fi):
                full_path = os.path.realpath(fi)
                full_path_splitted = full_path.split(os.path.sep)
                index = full_path_splitted.index('results-clean')
                section_label = full_path_splitted[index + 1]
                section_ordered_dict[section_label].append(fi)
            elif os.path.isfile(fi):
                section_ordered_dict[etc_label].append(fi)
        else:
            section_ordered_dict[section_label].append(fi)

    return section_ordered_dict


def create_tex(dirname, original_files=[], template_dir="", verbose=False):

    try:
        os.chdir(dirname)
    except:
        raise tex_creation_error('cannot chdir %s' % dirname)

    files_ordered_dict = collect_files('./')

    fmod = lambda x: x['is_modified'] is True
    texstr = ''

    allfiles = []
    hashsums = set()

    for section_label, files in sorted(files_ordered_dict.items()):
        # filter files
        files_filtered = []
        for fname in files:
            # check for own symlinks
            basename = os.path.basename(fname)
            if re.match('^' + symlink_prefix, basename):
                # print('symlink detected')
                pass

            # Informations about file named fname
            ftype = get_ftype(fname)
            md5s = md5sum(fname)
            is_duplicate = md5s in hashsums

            # MATLAB: Replace .asv with .m if both exist
            if (is_duplicate and Path(fname).suffix == '.m'):
                if Path(files_filtered[-1]['fname']).stem == Path(fname).stem:
                    files_filtered[-1]['is_duplicate'] = True
                    is_duplicate = False

            if not ftype == 'symbolic link':
                hashsums.add(md5s)
            is_modified = md5s not in original_files
            is_invalid = [lf(fname) for lf in invalid_filenames]
            omitted_flag = True in is_invalid
            timestamp = time.ctime(os.path.getmtime(fname))

            ### Dictionary with data for each file
            files_filtered.append({'fname': fname,
                                   'template-dir': template_dir,
                                   'ftype': ftype,
                                   'md5s': md5s,
                                   'is_modified': is_modified,
                                   'is_duplicate': is_duplicate,
                                   'timestamp' : timestamp,
                                   'omitted': omitted_flag})
        allfiles += files_filtered
        # create tex
        section_tex_str = ''
        for elem in filter(fmod, files_filtered):
            if elem['omitted'] is True or elem['is_duplicate'] is True:
                continue
            if elem['ftype'] in code_formats:
                if verbose:
                    print('   Code added: ' + elem['fname'])
                tmp = tex_listing(elem)
                section_tex_str += tmp
            elif elem['ftype'] in image_formats:
                if verbose:
                    print('   Picture added: ' + elem['fname'])
                section_tex_str += tex_figure(elem, dirname)
            elif elem['ftype'] in text_formats:
                if verbose:
                    print('   Text added: ' + elem['fname'])
                section_tex_str += tex_verb_listing(elem)
            else:
                elem['omitted'] = True
                if verbose:
                    print('   File omitted: ' + elem['fname'])

        if not section_tex_str == '':
            if section_label == '_____etc':
                texstr += '\\clearpage \n'
            else:
                texstr += '\n \\clearpage \\section{' + section_label.replace(
                    "_", "-") + '} \n'

        texstr += section_tex_str

    # Use pydf for this? See here: http://unix.stackexchange.com/questions/66418/how-to-merge-pdf-files-so-that-each-file-begins-on-an-odd-page-number
    #texstr += "\\AtEndDocument{\\clearpage\\ifodd\\value{page}\\else\\null\\clearpage\\fi}"
    texstr += '\n\\end{document}'
    return texstr, allfiles


def main(dirname, ip, identifier, original_files=[], lecture_title='', template_dir='', verbose=True,
         header = [ "fname", "ftype", "timestamp" ]):
    """
    Keyword Arguments:
    dirname         -- root directory
    identifier      -- Student name, nethz account
    original_files  -- list of md5 hashes of original files
    header          -- list of columns to display (in that order): possible: fname, ftype, timestamp, md5
    """
    # grab files in root directory
    cwd = os.getcwd()
    try:
        texstr, files = create_tex(dirname, original_files=original_files, template_dir=template_dir, verbose=verbose)
    except tex_creation_error:
        print('something went wrong in %s' % dirname)
        return

    os.chdir(cwd)

    print("tex-creator: found: %d files in %s" % (len(files), dirname))

    # create
    output_dir = os.path.join(os.path.abspath(dirname), '../')
    output_file = os.path.join(output_dir, '%s.tex' % ip)

    my_header = texheader.replace('IDENTIFIER', identifier)
    my_header = my_header.replace('IPADDRESS', ip)
    my_header = my_header.replace('LECTURETITLE', lecture_title)
    out = []

    # Create a header for type of file (given category identifier)
    def get_category_string(category, header):
        cat = {
                "unmodified" : "Unmodified",
                "modified" : "Modified",
                "duplicate" : "Modified (Duplicates)",
                "ignored" : "Ignored",
              }
        ncols = len(header);
        # Escape nightmare
        return r'\hline\multicolumn{{{}}}{{|l|}}{{\textbf{{{}}}}}\\\hline'.format(ncols, cat[category])


    def get_formatted_string(item, head):
        """
        Given column name, return appropriated formatted text.
        """
        if head == 'fname':
            return r'\verb`{' + item['fname'] + r'}`'
        elif head == "ftype":
            return short_names[item['ftype']]
        elif head == "timestamp":
            return str(item['timestamp'])
        elif head == "md5":
            return r'\verb`{'  + item['md5s'] + r'}`'

    # Given item, returns row of table representing item
    def get_formatted_item_string(item, header):
        template_str = ''
        for i, head in enumerate(header):
            template_str += get_formatted_string(item, head)
            if i+1 == len(header):
                template_str += "\\\\"
            else:
                template_str += "&"
        return template_str

    ncols = len(header);
    out.append(r'\begin{longtable}{' + '|l'*ncols + '|}')

    # modified
    out.append(get_category_string("modified", header))
    modified_files = filter(
        lambda x: (x['is_modified'] is True and x['is_duplicate'] is False
                   and x['omitted'] is False and not x['ftype'] == 'symbolic link'),
        files)
    for item in modified_files:
        out.append(get_formatted_item_string(item, header))

    # duplicate
    out.append(get_category_string("duplicate", header))
    for item in filter(
           lambda x: (x['is_modified'] is True and x['omitted'] is False
                       and x['is_duplicate'] is True and not x['ftype'] == 'symbolic link'),
            files):
        out.append(get_formatted_item_string(item, header))

    # not modified
    out.append(get_category_string("unmodified", header))
    for item in filter(
            lambda x: (x['is_modified'] is False and x['omitted'] is False
                       and not x['ftype'] == 'symbolic link'),
            files):
        out.append(get_formatted_item_string(item, header))

    # ignored
    out.append(get_category_string("ignored", header))
    for item in filter(
            lambda x: (x['omitted'] is True and not x['ftype'] == 'symbolic link'),
            files):
        out.append(get_formatted_item_string(item, header))
    out.append(r'\hline')
    out.append(r'\end{longtable}')

    # write to file
    if len(list(modified_files)) or identifier != 'not found':
        f = open(output_file, 'w')
        f.write(my_header)
        f.write('\n'.join(out))
        f.write(r'\pagebreak')
        f.write(texstr)
        f.close()
