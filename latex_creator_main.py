import argparse
import os, re
import subprocess
import latex_creator

exam_user_file='.exam-setup-user'

def read_info_file(infofile):
    with open(infofile) as f:
        for line in f.readlines():
            match = re.search('export LOGINNAME="(.+?)"', line)
            if match:
                loginname = match.group(1)
            match = re.search('export REALNAME="(.+?)"', line)
            if match:
                realname = match.group(1)

    return realname, loginname


def get_name_table():
    name_table = {}

    for dirname in os.listdir(args.results):
        match = re.match('[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}', dirname)
        if match:
            infofile = os.path.join(args.results, dirname, exam_user_file)
            if os.path.isfile(infofile):
                name_table[dirname] = read_info_file(infofile)

    return name_table


def get_file_hashes(hashfile):
    with open(hashfile) as f:
        hashes = [l[:32] for l in f.readlines()]
    return hashes


if __name__ == '__main__':
    results_dir = 'results-clean'

    parser = argparse.ArgumentParser()

    parser.add_argument("--results",
                        default="./",
                        help="""Directory containing the collected exam results.
                                Usually the result directory of:
                                  exam-setup --get-files
                        """)

    parser.add_argument("--lecture",
                        default="",
                        help="""Lecture title. It will be used as tex document author.
                        """)

    parser.add_argument("--hashes",
                        help="""File containing the md5 hashes of the original exam files.
                                Usually the output of:
                                  find ExamDir -type f -exec md5sum {} + > hashes.txt
                        """)

    parser.add_argument("--template-dir",
                        default="",
                        help="""Directory containing the template files.""")

    parser.add_argument("-v",
                        action='store_true',
                        help="""Verbose output
                        """)

    args = parser.parse_args()
    if args.hashes:
        file_hashes = get_file_hashes(args.hashes)
    else:
        file_hashes = []
    # Untabify, TODO this should also be applied to other files
    subprocess.call(["find", ".", "-name", "'*.py'", "-exec", "sed -i 's/\t/    /g'", "{}", ";"])


    print("Template dir is: " + args.template_dir)
    # Latexify, check if this is already a leaf directory
    regex = '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'
    if re.match(regex, os.path.split(args.results)[-1]) and os.path.isfile(os.path.join(args.results, '.exam-setup-user')):
        ip = os.path.split(args.results)[-1]

        name, nethz = read_info_file(os.path.join(args.results, '.exam-setup-user'))
        student = "%s (%s)" % (name, nethz)
        latex_creator.main(os.path.join(args.results, 'results-clean'), ip, student,
                           template_dir=args.template_dir,
                           original_files=file_hashes, lecture_title=args.lecture, verbose=args.v)
    else:
        name_table = get_name_table()
        # args.results is not a leaf directory, search recursively
        for dirname, subdirs, files in os.walk(args.results):
            for si in subdirs:
                match = re.match(regex, si)
                if match:
                    if args.v:
                        print('========================================')
                        print('Found student directory: ' + si)
                    local_result_dir = os.path.join(args.results, si, 'results-clean')
                    ip = si
                    if si in name_table:
                        name, nethz = name_table[si]
                        student = "%s (%s)" % (name, nethz)
                    else:
                        student = 'not found'

                    latex_creator.main(local_result_dir, ip, student, original_files=file_hashes,
                                       template_dir=args.template_dir,
                                       lecture_title=args.lecture, verbose=args.v)
