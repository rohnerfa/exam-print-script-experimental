#!/usr/bin/zsh -f

# fail when an error is encountered.
set -e

# Send bug reports to simon.pintarelli@sam.math.ethz.ch

zparseopts -E -D -A myoptions t c s v -help -clean -lecture:=olec

# t: produce tex files
# c: compile PDFs
# s: concatenate PDFs in a single file
# v: verbose output

# info file name
EXAM_SETUP_USER=.exam-setup-user



show_help() {
    echo "--\nusage: "
    echo
    echo " $1 [-tcsv] [--lecture 'Numerical Methods'] /path/to/results [/path/to/resources]"
    echo
    echo " - create tex-files & compile"
    echo "    $1 --lecture 'Numerical Methods' /path/to/results /path/to/resources"
    echo
    echo " - create tex-files *only*:"
    echo "    $1 -t --lecture 'Numerical Methods' /path/to/results /path/to/resources"
    echo
    echo " - compile tex-files using xelatex:"
    echo "    $1 -c /path/to/results [/path/to/resources]"
    echo
    echo " - concatenate all PDFs to results.pdf in /path/to/results:"
    echo "    $1 -s /path/to/results [/path/to/resources]"
    echo
    echo "/path/to/results  : student submissions (folder with ip folders)"
    echo "/path/to/resources: template files (i.e. folder 'questions')"
    echo "--"
}

type parallel > /dev/null
if [[ ! $? -eq 0 ]]; then
    echo "Cannot find parallel command. Make sure you have gnu parallel installed. Exiting."
    exit 1
fi

if (( $+myoptions[--help] )) ; then
    show_help $0
    exit 0
fi

if [[ $# -eq 0 ]]; then
    show_help $0
    exit 1
fi

if (( $+myoptions[-t] + $+myoptions[-s] + $+myoptions[-c] == 0 )); then
    # default is to create tex-files (-t) and compile the PDFs (-c)
    myoptions[-t]=
    myoptions[-c]=
fi

if  [[ $# -eq 1 ]]; then
    if (($+myoptions[-t] )); then
        echo "\n"
        echo "*WARNING* resources not specified."
        echo "\n"
    fi
    results=$(realpath $1)
elif [[ $# -eq 2 ]]; then
    results=$(realpath $1)
    resources=$(realpath $2)
fi

if [[ $results == "" && $resources == "" ]]; then
    show_help $0
    echo "Exiting."
    exit 1
fi

source_dir=${$(realpath $0):h}
# make sure that python can find the source files
export EXAM_PRINT_SCRIPT_SOURCE_DIR=$source_dir
echo "Results:   ${results}"
echo "Resources: ${resources}"
echo

ls $results/**/${EXAM_SETUP_USER} > /dev/null
if [[ ! $? -eq 0 || ! -d ${results} ]]; then
    echo "*ERROR* ${results} does not contain ${EXAM_SETUP_USER} files! You might be using a wrong path."
    show_help $0
    echo "Exiting."
    exit 1
fi

if (( $+myoptions[-v] )); then
    verbose=-v
fi


# remove auxiliary tex files
if (( $+myoptions[--clean] )) ; then
    (cd $results && rm **/*{log,aux,toc})
fi

# create tex-files
if (( $+myoptions[-t] )) ; then
    echo "\n>>> Creating tex-files ...\n"
    lecture_title=${olec#--lecture}
    if [[ $lecture_title == "" ]]; then
        echo "*WARNING* Lecture title not set."
        lecture_title="Exam"
    fi

    if (( $+myoptions[-v])); then
        # verbose mode: run in serial
        if [[ ! -z ${resources+x} && -d $resources ]]; then
            echo "using hashes"
            python2 $source_dir/latex_creator_main.py --hashes <(cd $resources && md5sum **/*(.)) --results $results --lecture $lecture_title --template-dir ${resources} $verbose
        else
            python2 $source_dir/latex_creator_main.py --results $results --lecture $lecture_title $verbose
        fi || exit 1
    else
        # non verbose mode run in parallel
        results_dir=$(print -l ${results}/**/${EXAM_SETUP_USER}(:h))
        echo ${results_dir} > ~/foo
        if [[ ! -z ${resources+x} && -d $resources ]]; then
            hashes=$(cd $resources && md5sum **/*(.))
            tmpfile=$(mktemp)
            echo $hashes  > $tmpfile
            echo "using hashes"
            echo ${results_dir} | parallel --bar --will-cite python2 $source_dir/latex_creator_main.py --hashes $tmpfile --results {} --template-dir ${resources} --lecture \'${lecture_title}\' >> ${results}/python.log
            rm $tmpfile
        else
            echo ${results_dir} | parallel --bar --will-cite python2 $source_dir/latex_creator_main.py --results {} --lecture \'${lecture_title}\' $verbose >> ${results}/python.log
        fi || exit 1
    fi
fi

# compile
if (( $+myoptions[-c] )) ; then
    if [[ ! -e $HOME/.fonts/Inconsolata-Regular.ttf ]]; then
        echo "Installing Inconsolata-{Bold,Regular}.ttf to $HOME/.fonts"
        mkdir -p $HOME/.fonts && cp ${source_dir}/Inconsolata-{Bold,Regular}.ttf $HOME/.fonts/
    fi
    echo "\n>>> Compiling PDFs in $results ... \n"
    (
        cd $results
        # get all subfolders containing the EXAM_SETUP_USER file
        results_dir=$(print -l **/${EXAM_SETUP_USER}(:h))
        files=$(for ldir in ${(f)results_dir}; do
                   tail=${ldir:t}
                   texfile=${ldir}/${tail}.tex

                   if [[ -f "$texfile" ]]; then
                       echo "$texfile"
                   else
                       (echo "*WARNING* $texfile does not exist." 1>&2)
                   fi
                done)

        if (( $+myoptions[-v] )); then
            redirect=/dev/stdout
        else
            redirect=/dev/null
        fi

        echo $files | parallel --halt soon,fail=1 --bar --will-cite xelatex '-interaction nonstopmode -halt-on-error' > $redirect
        echo $files | parallel --halt soon,fail=1 --bar --will-cite xelatex '-interaction nonstopmode -halt-on-error' > $redirect
        # check if pdf was created
        for ldir in  ${(f)results_dir}; do
            tail=${ldir:t}
            texfile=${ldir}/${tail}.tex
            pdfpath=${texfile:t:r}
            if [[ ! -f "$pdfpath.pdf" ]]; then
                (echo "*WARNING* no PDF created for $ldir" 1>&2)
            fi
        done

    )
fi

# concatenate PDFs
if (( $+myoptions[-s] )) ; then
    (cd $results && pdfunite [0-9]*pdf results.pdf)
fi

# remove auxiliary tex files
if (( $+myoptions[--clean] )) ; then
    mkdir -p $results/logs/;
    (cd $results && mv **/*{log,aux,toc} $results/logs/)
fi