Only works with Python >= 3.5, TeXLive 2023 (tested) and newer.
On macOS, you need to install the ``diffutils`` and ``parallel`` package from ``brew``.


This is a collection of Python scripts intended to simplify the process of
printing computer exam submissions. It will generated nicely formatted LaTeX
code listings, sub-directories are mapped to sections and include plots in
``PDF`` or ``png`` format. If a path to the template directory is given, it can
skip unmodified files and highlight changes made by the students. Currently, there is
language support for ``Python``, ``Matlab/Octave`` and ``C++``.

USAGE
=====

0) Installation
^^^^^^^^^^^^^^^

Open a shell and clone the ``git`` repository:

::

   cd ~ # or any other path of your choice.
   git clone https://gitlab.math.ethz.ch/A-SAM/exam-print-script.git

This will give you a directory called ``exam-print-script``.


1) Create the tex-files and compile the PDFs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ./run.sh  --lecture 'Numerische Methoden PHYS' /path/to/results [/path/to/resources]

The first argument ``/path/to/results`` should contain the student submissions.
The second (optional) argument ``/path/to/resources`` should contain the
original template files. It is *strongly* recommended to use it: unmodified
files will be omitted in the printout and modified lines will be highlighted with a
green background.

The pdf-files are saved at ``/path/to/results``.


If you're printing a large exam, send the job to the `Druckzentrum
<https://www.shops.ethz.ch/dienste/anwendungen/druckversand/scripts/main.php>`_.
They can also print from many single files (and staple automatically!), the
print quality is much better than that of the D-MATH printers. Don't use the web
form for the upload, it will ask you to upload a single file, which implies that
you have to staple/sort manually. Call them by phone or write an email to
druckzentrum_hg@services.ethz.ch and agree how to send the PDFs. Recommended
printer setting is 2 on-one (2-up), duplex (long-edge binding) and staple
(top-left).

The print jobs sent to the Druckzentrum may take a few hours up to half a day to
complete. If you don't have time to wait, you can print the exams on one of the
office printers. See below how to do that.

Hint: You can run the stages for creation of the tex-files and compilation separately,
see. ``./run.sh --help``.

Hint: LaTeX compilation is slow, if there are many students, e.g.
>100, the script will run much faster if you use one of the ada-servers (copy
the files to ``/userdata``, e.g. not to your home directory).



2a) Concatenate the PDFs into one PDF with two sides per page
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

	./concatenate.py /path/to/results

The main output is a single pdf-file called ``out_rotated.pdf``,
which will be saved at ``/path/to/results``.
This file contains all students' exams.
Each page includes two sides of the original pdf-files.
Blank pages are added so that the exam of each students starts with an odd page number.
Every even page is rotated by 180°.

``out_rotated.pdf`` is in a conventient format for printing, as the sheets of the individual exams can be separated without overlaps.

Additionally, other outputs are created at ``/path/to/results``:

- A single pdf-file called ``out.pdf``, which is the same as ``out_rotated.pdf`` without the rotation of even pages.
The default printing settings (duplex printing, long side) already make sure that the pages of the printout are properly oriented.

- For each student a pdf-file ending with ``-nup.pdf`` is created, same as the original pdf-file from ``run.sh`` but with two sides per page.

2b) Alternatively, send the individual PDFs to a printer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ./print.py /path/to/results

This will print the pdfs in alphabetical order by last name.
Exceptions of the ordering are students that write their first
name where they should have given their last name,
and last names containing non-ascii characters.
By default the printer in front of HG J54 will be used.
It will print two-pages up with long edge bindings,
and each document stapled (if the selected printer has a stapling feature!).
Please configure print.py according to your needs through the variables
``LPR_OPTIONS`` and ``PRINTER``.
See the documentation of ``print.py`` for more information.
It is wise to first use the script on a small test batch,
i.e. by passing a ``/path/to/results`` that contains only a few pdfs.

- Hint: Use ``lpstat -p`` to get a list of available printers.



..
   Manual procedure: workaround if wrapper scripts ``run.sh`` / ``print.py`` don't work
   ------------------------------------------------------------------------------------

   In case the wrapper scripts don't work, you can fallback to the manual procedure described here.

   1) Compute the hashes
   ^^^^^^^^^^^^^^^^^^^^^

   ::

       find path/to/the/resources/ -type f -exec md5sum {} + > hashes.txt

   This is necessary to detect files that have not been modified by the students.
   The directory ``resources`` should be identical to the ``resources`` directory
   initially distributed to the students computers. Adapt the path to your own setting.

   2) Adapt the header as necessary
   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   ::

       emacs header.txt

   Adapt the lecture title in ``\author{LECTURETITLE}`` on line 63.
   Do *not* change the line ``\title{IDENTIFIER \\ IPADDRESS }`` above.

   3) Run this tool
   ^^^^^^^^^^^^^^^^

   ::

       python latex_creator_main.py --hashes path/to/the/hashes.txt --results path/to/the/results/

   Specify the path to the ``hashes.txt`` file from step 1 and the path to the directory
   containing all the results. This directory should contain one sub-directory per student,
   named by IP address. Just unpack the ``tar`` file you get from ``exam-setup --compress-result-folders``
   and point the path to the content. The ``tex`` files will be generated within the student's
   directories.

   Note: This tool is not (yet) Python 3 compliant.

   4) Run XeLaTeX
   ^^^^^^^^^^^^^^

   Go to the ``results`` directory from step 3 and run ``xelatex`` like shown below:

   ::

       cd path/to/the/results/
       for f in `ls */*.tex`; do xelatex $f; xelatex $f; done

   The ``pdf`` files will appear in the ``results`` directory (from here we ran ``xelatex``) and
   *not* within the student's directories.

   Note: The ``tex`` files will *not* compile with just ``latex`` or ``pdflatex`` due to the use of more modern features.

   5) Print the pdf-files
   ^^^^^^^^^^^^^^^^^^^^^^

   Send *all* the pdf-files to a printer:

   ::

       cd path/to/the/results/
       for f in *.pdf; do lpr -P p-hg-j-42 -o sides=two-sided-long-edge -o landscape -o fit-to-page -o media=A4 -o number-up=2 -o number-up-layout=lr $f; done

   This will send one print job per student (``pdf`` file) to the printer specified.
   On the J-floor, the printers available are ``p-hg-j-42`` and ``p-hg-j-54``.
   You may want to adapt the printer or the print settings. See ``man lpr`` and ``man lpoptions`` for more details about printing.

   Warning: Printing may take a very long time. You may want to print in batches or on different printers at the same time.



TODO
====

* Replace linux file command by xdg-mime to determine filetypes
* Add support for more programming languages (currently, there is support for
  ``C++``, ``Python`` and ``Matlab``)


Contributors
============

Raoul Bourquin,
Filippo Leonardi,
Luc Grosheintz,
Simon Pintarelli,
Daniele Casati,
Tandri Gauksson.
